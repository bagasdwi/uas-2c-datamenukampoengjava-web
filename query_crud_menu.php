<?php

$DB_NAME = "andro_menu";
$DB_USER = "root";
$DB_PASS = "";
$DB_SERVER_LOC = "localhost";

if($_SERVER['REQUEST_METHOD'] == 'POST'){
    $conn = mysqli_connect($DB_SERVER_LOC,$DB_USER,$DB_PASS,$DB_NAME);
    $mode = $_POST['mode'];
    $respon = array(); $respon['kode'] = '000';
    switch($mode){
        case "insert":
            $id_menu = $_POST['id_menu'];
            $nama = $_POST['nama'];
            $nama_satuan = $_POST['nama_satuan'];
            $nama_kategori = $_POST['nama_kategori'];
            $harga = $_POST['harga'];
            $imstr = $_POST['image'];
            $file = $_POST['file'];
            $path = "web_android/public/images/";

            $sql = "select id_satuan, id_kategori from satuan, kategori where nama_satuan = '$nama_satuan' and nama_kategori = '$nama_kategori'";
            $result = mysqli_query($conn, $sql);
            if(mysqli_num_rows($result)>0){
                $data = mysqli_fetch_assoc($result);
                $id_satuan = $data['id_satuan'];
                $id_kategori = $data['id_kategori'];

                $sql = "insert into menu(id_menu, nama, id_satuan, id_kategori, harga, photos) values(
                    '$id_menu','$nama','$id_satuan','$id_kategori','$harga' ,'$file'
                )";
                $result = mysqli_query($conn,$sql);
                if($result){
                    if(file_put_contents($path.$file,base64_decode($imstr)) == false){
                        $sql = "delete from menu where id_menu = '$id_menu'";
                        mysqli_query($conn,$sql);
                        $respon['kode'] = "111";
                        echo json_encode($respon); exit();
                    }else{
                        echo json_encode($respon); exit();
                    }
                }else{
                    $respon['kode'] = "111";
                    echo json_encode($respon); exit();
                }
            }
        break;
        case "update":
            $id_menu = $_POST['id_menu'];
            $nama = $_POST['nama'];
            $nama_satuan = $_POST['nama_satuan'];
            $nama_kategori = $_POST['nama_kategori'];
            $harga = $_POST['harga'];
            $imstr = $_POST['image'];
            $file = $_POST['file'];
            $path = "web_android/public/images/";

            $sql = "select id_satuan, id_kategori from satuan, kategori where nama_satuan = '$nama_satuan' and nama_kategori = '$nama_kategori'";
            $result = mysqli_query($conn, $sql);

            if(mysqli_num_rows($result)>0){
                $data = mysqli_fetch_assoc($result);
                $id_satuan = $data['id_satuan'];
                $id_kategori = $data['id_kategori'];

                $sql = "";
                if($imstr == ""){
                    $sql = "update menu set nama='$nama', id_satuan='$id_satuan', id_kategori='$id_kategori', harga='$harga' where id_menu='$id_menu'";
                    $result = mysqli_query($conn,$sql);
                    if($result){
                        echo json_encode($respon); exit();
                    }else{
                        $respon['kode'] = "111";
                        echo json_encode($respon); exit();
                    }
                }else{
                    if(file_put_contents($path.$file,base64_decode($imstr)) == false){
                        $respon['kode'] = "111";
                        echo json_encode($respon); exit();
                    }else{
                        $sql = "update menu set nama='$nama', id_satuan='$id_satuan', id_kategori='$id_kategori', harga='$harga' , photos='$file' where id_menu='$id_menu'";
                        $result = mysqli_query($conn,$sql);
                        if($result){
                            echo json_encode($respon); exit();
                        }else{
                            $respon['kode'] = "111";
                            echo json_encode($respon); exit();
                        }
                    }
                }
            }
        break;
        case "delete":
            $id_menu = $_POST['id_menu'];
            $sql = "select photos from menu where id_menu = '$id_menu'";
            $result = mysqli_query($conn,$sql);
            if($result){
                if(mysqli_num_rows($result)>0){
                    $data = mysqli_fetch_assoc($result);
                    $photos = $data['photos'];
                    $path = "web_android/public/images/";
                    unlink($path.$photos);
                }
                $sql = "delete from menu where id_menu = '$id_menu'";
                $result = mysqli_query($conn,$sql);
                if($result){
                    echo json_encode($respon); exit();
                }else{
                    $respon['kode'] = '111';
                    echo json_encode($respon); exit();
                }
            }
        break;
    }
}