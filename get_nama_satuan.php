<?php 

    $DB_NAME = "andro_menu";
    $DB_USER = "root";
    $DB_PASS = "";
    $DB_SERVER_LOC = "localhost";

    if($_SERVER['REQUEST_METHOD'] == 'POST'){
        $conn = mysqli_connect($DB_SERVER_LOC,$DB_USER,$DB_PASS,$DB_NAME);
        $nama_satuan = $_POST['nama_satuan'];
        if(empty($nama_satuan)){
            $sql = "select * from satuan order by id_satuan asc";
        }else{
            $sql = "select * from satuan where nama_satuan like '%$nama_satuan%' order by id_satuan asc";
        }
        $result = mysqli_query($conn,$sql);
        if(mysqli_num_rows($result) > 0){
            header("Access-Control-Allow-Origin: *");
            header("Content-type: application/json; charset=UTF-8");

            $nama_satuan = array();
            while($nm_satuan = mysqli_fetch_assoc($result)){
                array_push($nama_satuan,$nm_satuan);
            }
            echo json_encode($nama_satuan);
        }
    }