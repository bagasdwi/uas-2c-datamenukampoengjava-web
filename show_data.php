<?php

$DB_NAME = "andro_menu";
$DB_USER = "root";
$DB_PASS = "";
$DB_SERVER_LOC = "localhost";

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $conn = mysqli_connect($DB_SERVER_LOC, $DB_USER, $DB_PASS, $DB_NAME);
    $nama = $_POST['nama'];
    $sql = "SELECT m.id_menu, m.nama, k.nama_kategori, s.nama_satuan,  m.harga, concat('http://192.168.43.94/uas_android/web_android/public/images/',m.photos) as url
            FROM menu m, satuan s, kategori k
            WHERE m.id_satuan = s.id_satuan AND m.id_kategori = k.id_kategori
            and m.nama like '%$nama%'";
    $result = mysqli_query($conn, $sql);
    if (mysqli_num_rows($result) > 0) {
        header("Access-Control-Allow-Origin: *");
        header("Content-type: application/json; charset=UTF-8");

        $data_mn = array();
        while ($mn = mysqli_fetch_assoc($result)) {
            array_push($data_mn, $mn);
        }
        echo json_encode($data_mn);
    }
}
