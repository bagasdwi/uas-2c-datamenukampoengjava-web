<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('admin.dashboard.index');
});
	Route::get('/home', 'HomeController@index')->name('home');
    Route::resource('/menu', 'Admin\Menu\MenuController');
    Route::resource('/satuan', 'Admin\Satuan\SatuanController');
    Route::resource('/kategori', 'Admin\Kategori\KategoriController');
