<?php

namespace App\Http\Controllers\Admin\Menu;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Menu;
use App\Models\Satuan;
use App\Models\Kategori;


class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $menu = Menu::all();
        return view('admin.menu.index', compact('menu'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $satuan = Satuan::all();
        $kategori = Kategori::all();
        return view('admin.menu.create', compact('satuan','kategori'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $menu = new Menu();

        $menu->id_menu = $request->input('id_menu');
        $menu->nama = $request->input('nama');
        $menu->id_kategori = $request->input('id_kategori');
        $menu->id_satuan = $request->input('id_satuan');
        $menu->harga = $request->input('harga');

            if ($request->hasfile('photos')) {
                $file = $request->file('photos');
                $extension = $file->getClientOriginalExtension();
                $filename = 'DC'.date('YmdHis') . '.' . $extension;
                $file->move('images/', $filename);
                $menu->photos = $filename;
            } else{
                return $request;
                $menu->photos = '';
            }
            $menu->timestamps = false;
            $menu->save();


        \Session::flash('sukses','Data Berhasil Ditambahkan');  
        return redirect('/menu');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id_menu
     * @return \Illuminate\Http\Response
     */
    public function show($id_menu)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id_menu
     * @return \Illuminate\Http\Response
     */
    public function edit($id_menu)
    {
        $satuan = Satuan::all();
        $kategori = Kategori::all();
        $menu = Menu::findorfail($id_menu);
        return view('admin.menu.edit', compact('menu','satuan', 'kategori'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id_menu
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id_menu)
    {
        $menu = Menu::findorfail($id_menu);

        $menu->id_menu = $request->input('id_menu');
        $menu->nama = $request->input('nama');
        $menu->id_kategori = $request->input('id_kategori');
        $menu->id_satuan = $request->input('id_satuan');
        $menu->harga = $request->input('harga');

            if ($request->hasfile('photos')) {
                $file = $request->file('photos');
                $extension = $file->getClientOriginalExtension();
                $filename = 'DC'.date('YmdHis') . '.' . $extension;
                $file->move('images/', $filename);
                $menu->photos = $filename;
            }
            $menu->timestamps = false;
            $menu->save();


        \Session::flash('sukses','Data Berhasil Diupdate');  
        return redirect('/menu');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id_menu
     * @return \Illuminate\Http\Response
     */
    public function destroy($id_menu)
    {
        $menu = Menu::findorfail($id_menu);
        $menu->delete();

        \Session::flash('sukses','Data Berhasil Dihapus');
        return redirect('/menu');
    }
}
