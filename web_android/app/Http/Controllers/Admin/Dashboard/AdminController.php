<?php

namespace App\Http\Controllers\Admin\Dashboard;

use Illuminate\Http\Request;
use App\Models\User;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    public function dashboard()
    {
        return view('admin.dashboard.index');
    }
    public function detail_user()
    {
        return view('admin.user.detail_user');
    }
    public function data_user()
    {
    	$user = User::all();
        return view('admin.user.data_user',compact('user'));
    }
    public function destroy($id)
    {
        $user = User::findorfail($id);
        $user->delete();

        \Session::flash('sukses','Data Berhasil Dihapus');
        return redirect('/data_user');
    }
}
