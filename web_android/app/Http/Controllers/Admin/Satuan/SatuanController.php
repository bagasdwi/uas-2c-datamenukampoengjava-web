<?php

namespace App\Http\Controllers\Admin\Satuan;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Satuan;


class SatuanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $satuan = Satuan::all();
        return view('admin.satuan.index', compact('satuan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.satuan.create');    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $satuan = new Satuan();

        $satuan->id_satuan = $request->input('id_satuan');
        $satuan->nama_satuan = $request->input('nama_satuan');
        $satuan->timestamps = false;
        $satuan->save();

        \Session::flash('sukses','Data Berhasil Ditambahkan');
        return redirect('/satuan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id_satuan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id_satuan)
    {
        $satuan = Satuan::findorfail($id_satuan);
        return view('admin.satuan.edit', compact('satuan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id_satuan)
    {
        $satuan = Satuan::findorfail($id_satuan);

        $satuan->id_satuan = $request->input('id_satuan');
        $satuan->nama_satuan = $request->input('nama_satuan');
        $satuan->timestamps = false;
        $satuan->save();

        \Session::flash('sukses','Data Berhasil Diupdate');
        return redirect('/satuan');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id_satuan)
    {
        $satuan = Satuan::findorfail($id_satuan);
        $satuan->delete();

        \Session::flash('sukses','Data Berhasil Dihapus');
        return redirect('/satuan');
    }
}
