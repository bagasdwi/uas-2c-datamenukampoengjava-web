<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Satuan extends Model
{
    protected $table = 'satuan';
    protected $fillable = ['id_satuan','nama_satuan'];
    protected $primaryKey = 'id_satuan';
}
