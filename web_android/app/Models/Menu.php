<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $table = 'menu';
    protected $fillable = ['id_menu','nama','id_satuan','id_kategori','harga','photos'];
    protected $primaryKey = 'id_menu';
    
    public function satuan()
    {
        return $this->belongsTo('App\Models\Satuan','id_satuan');
    }
    
    public function kategori()
    {
        return $this->belongsTo('App\Models\Kategori','id_kategori');
    }
}