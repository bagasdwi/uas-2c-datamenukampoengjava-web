@extends('layouts.master')
@section('sub-judul','Edit Satuan')
@section('jejak','Edit Satuan')
@section('content')

<!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <!-- /.card-header -->
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-12">
                <thead>
                    <form action="{{ route('satuan.update', $satuan->id_satuan ) }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('patch')
                    <thead>
                    <div class="form-group">
                        <label >Id Satuan</label>
                        <input name="id_satuan" type="text" value="{{ $satuan->id_satuan }}" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Id Satuan" value="">
                    </div>
                    <div class="form-group">
                        <label >Nama Satuan</label>
                        <input name="nama_satuan" type="text" value="{{ $satuan->nama_satuan }}" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Nama Satuan" value="">
                     </div>
                        <button type="submit" class="btn btn-success">Update</button>
                  </form>
                </thead>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection