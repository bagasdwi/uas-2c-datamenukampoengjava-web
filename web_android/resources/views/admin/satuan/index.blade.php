@extends('layouts.master')
@section('sub-judul','Satuan')
@section('jejak','Satuan')
@section('content')

<!-- Main content -->
    <section class="content">
      <div class="row">
          <div class="col-12">
        <!-- SELECT2 EXAMPLE -->
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Data Satuan</h3>
            <div class="text-right">
                <a href="{{ route('satuan.create') }}" class="btn btn-success btn-sm float-leftt">
                    <i class="fas fa-plus"></i>
                </a>
            </div>
          </div>
            <!-- /.card-header -->
            <div class="card-body">
              <div id="example1_wrapper" class="dataTables_wrapper dt-bootstrap4">
                  <div class="row"><div class="col-sm-12 col-md-6">
                        <div class="dataTables_length" id="example1_length">
                            
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6">
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">

                <table id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                <thead>
                <tr>
                  <th>Id Satuan</th>
                  <th>Nama Satuan</th>
                  <th>Opsi</th>
                </tr>
            </thead>
                <tbody>
                @php $no = 1; 
                @endphp
                @foreach ($satuan as $result => $hasil)
                <tr>
                    <td>{{ $hasil->id_satuan}}</td>
                    <td>{{ $hasil->nama_satuan}}</td>
                    <td>
                      <form action="{{ route('satuan.destroy', $hasil->id_satuan) }}" method="POST">
                        @csrf
                        @method('delete')
                        <a href="{{ route('satuan.edit', $hasil->id_satuan) }}" class="btn btn-info btn-sm">
                          <i class="fas fa-edit"></i>
                        </a>
                        <button type="submit" class="btn btn-danger btn-sm" onClick="return confirm('Yakin Mau Dihapus')">
                          <i class="fas fa-trash"></i>
                        </button>
                      </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
            </table>
            </div>
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection