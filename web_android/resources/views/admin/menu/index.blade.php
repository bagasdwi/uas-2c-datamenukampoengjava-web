@extends('layouts.master')
@section('sub-judul','Menu')
@section('jejak','Menu')
@section('content')

<!-- Main content -->
    <section class="content">
        <div class="row">
          <div class="col-12">
        <!-- SELECT2 EXAMPLE -->
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Data Menu</h3>
            <div class="text-right">
                <a href="{{ route('menu.create') }}" class="btn btn-success btn-sm float-leftt">
                    <i class="fas fa-plus"></i>
                </a>
            </div>
          </div>
            <!-- /.card-header -->
            <div class="card-body">
              <div id="example1_wrapper" class="dataTables_wrapper dt-bootstrap4">
                  <div class="row"><div class="col-sm-12 col-md-6">
                        <div class="dataTables_length" id="example1_length">
                            
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6">
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">

                <table id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                <thead>
                <tr>
                    <th>Id Menu</th>
                    <th>Nama Menu</th>
                    <th>Kategori</th>
                    <th>Satuan</th>
                    <th>Harga</th>
                    <th>Foto</th>
                    <th>Opsi</th>
                </tr>
            </thead>
                <tbody>
                @php 
                $no = 1; 
                @endphp
                @foreach ($menu as $result => $hasil)
                <tr>
                    <td>{{ $hasil->id_menu }}</td>
                    <td>{{ $hasil->nama}}</td>
                    <td>{{ $hasil->kategori->nama_kategori}}</td>
                    <td>{{ $hasil->satuan->nama_satuan}}</td>
                    <td>Rp. {{number_format($hasil->harga)}}</td>
                    <td><img src="images/<?php echo $hasil['photos']; ?>" style="width: 100px;"></td>
                    <td>
                      <form action="{{ route('menu.destroy', $hasil->id_menu) }}" method="POST">
                        @csrf
                        @method('delete')
                        <a href="{{ route('menu.edit', $hasil->id_menu) }}" class="btn btn-info btn-sm">
                          <i class="fas fa-edit"></i>
                        </a>
                        <button type="submit" class="btn btn-danger btn-sm" onClick="return confirm('Yakin Mau Dihapus')">
                          <i class="fas fa-trash"></i>
                        </button>
                      </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
            </table>
            </div>
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection