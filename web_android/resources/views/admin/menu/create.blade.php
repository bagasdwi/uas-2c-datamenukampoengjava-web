@extends('layouts.master')
@section('sub-judul','Tambah Menu')
@section('jejak','Tambah Menu')
@section('content')

<!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <!-- /.card-header -->
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-12">
            		<thead>
                		<form action="{{ route('menu.store') }}" method="POST" enctype="multipart/form-data">
                		@csrf
                		<thead>
                		<div class="form-group">
                    		<label >Id Menu</label>
                    		<input name="id_menu" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Id Menu" value="">
               			 </div>
                     <div class="form-group">
                        <label >Nama Menu</label>
                        <input name="nama" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Nama Menu" value="">
                     </div>
               			 <div class="form-group">
                    		<label>Satuan</label>
                    		<select class="form-control" name="id_satuan">
                    			<option value="" holder>Pilih Satuan</option>
                    			@foreach($satuan as $result)
                    			<option value="{{ $result['id_satuan']}}">{{ $result->nama_satuan }}</option>
                    			@endforeach
                    		</select>
               			 </div>
               			 <div class="form-group">
                    		<label >Kategori</label>
                    		<select class="form-control" name="id_kategori">
                    			<option value="" holder>Pilih Kategori</option>
                    			@foreach($kategori as $result)
                    			<option value="{{ $result['id_kategori']}}">{{ $result->nama_kategori}}</option>
                    			@endforeach
                    		</select>
               			 </div>
                     <div class="form-group">
                        <label >Harga</label>
                        <input name="harga" type="number" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Harga" value="">
                     </div>
                     <div class="form-group">
                        <label >Foto</label>
                        <input name="photos" type="file" class="form-control">
                     </div>
                    		<button type="submit" class="btn btn-success">Tambah</button>
            			</form>
            		</thead>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection