@extends('layouts.master')
@section('sub-judul','Edit Menu')
@section('jejak','Edit Menu')
@section('content')

<!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <!-- /.card-header -->
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-12">
                <thead>
                    <form action="{{ route('menu.update', $menu->id_menu ) }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('patch')
                    <thead>
                      <div class="form-group">
                        <label >Id Menu</label>
                        <input name="id_menu" type="text" value="{{ $menu->id_menu }}" class="form-control">
                     </div>
                    <div class="form-group">
                        <label >Nama Menu</label>
                        <input name="nama" type="text" value="{{ $menu->nama }}" class="form-control">
                     </div>
                     <div class="form-group">
                    		<label >Satuan</label>
                    		<select class="form-control" name="id_satuan">
                    			<option holder>Pilih Satuan</option>
                    			@foreach($satuan as $result)
                    			<option value="{{ $result['id_satuan']}}" 
                    			@if($menu->id_satuan == $result->id_satuan)
                    				selected
                    			@endif>{{ $result->nama_satuan }}</option>
                    			@endforeach
                    		</select>
               			 </div>
               			 <div class="form-group">
                    		<label >Kategori</label>
                    		<select class="form-control" name="id_kategori">
                    			<option holder>Pilih Kategori</option>
                    			@foreach($kategori as $result)
                    			<option value="{{ $result['id_kategori']}}"
                    			@if($menu->id_kategori == $result->id_kategori)
                    				selected
                    			@endif
                    			>{{ $result->nama_kategori}}</option>

                    			@endforeach
                    		</select>
               			 </div>
                     <div class="form-group">
                        <label >Harga</label>
                        <input name="harga" type="number" value="{{ $menu->harga }}" class="form-control">
                     </div>
                     <div class="form-group">
                        <label >Foto</label>
                        <input name="photos" type="file" value="{{ $menu->photos }}" class="form-control">
                     </div>
                        <button type="submit" class="btn btn-success">Update</button>
                  </form>
                </thead>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection