@extends('layouts.master')
@section('sub-judul','Home')
@section('content')

    <!-- Main content -->
    <section class="content">
      <div class="row">
          <div class="col-12">
        <!-- SELECT2 EXAMPLE -->
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Kampoeng Java</h3>
            <div class="text-right">
            </div>
          </div>

      <!-- Default box -->
        <div class="card-body">
          <div class="row">
            <div class="col-12">
              <div class="col-12">
                <img src="{{asset('images/home.jpg')}}" class="product-image" alt="Product Image">
              </div>
            </div>
            <div class="col-12">
              <center>
                <h2 class="my-3">Kampoeng Java</h2>
              </center>
            </div>
          </div>
          <div class="row mt-4">
            <nav class="w-100">
              <div class="nav nav-tabs" id="product-tab" role="tablist">
                <a class="nav-item nav-link active" id="product-desc-tab" data-toggle="tab" href="#product-desc" role="tab" aria-controls="product-desc" aria-selected="true">Deskripsi</a>
              </div>
            </nav>
            <div class="tab-content p-3" id="nav-tabContent">
              <div class="tab-pane fade show active" id="product-desc" role="tabpanel" aria-labelledby="product-desc-tab">
                <h5>Apasih Kampoeng Java itu?</h5>Kampoeng Java Resto and Cafe merupakan suatu usaha kuliner yang terletak di Perum Kadiri palace jalan dandang gendis kecamatan ngasem desa Gogorante Kediri yang bergerak di bidang penjualan makanan dan minuman. Kampoeng Java Resto and Cafe memiliki 5 orang pegawai dan memiliki menu khas yang diberikan adalah masakan jawa yang diolah sedemikian rupa sehingga dapat disajikan dengan menarik.</div>
            </div>
          </div>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->

@endsection