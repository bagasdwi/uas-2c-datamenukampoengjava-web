@extends('layouts.master')
@section('sub-judul','Tambah Kategori')
@section('jejak','Tambah Kategori')
@section('content')

<!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <!-- /.card-header -->
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-12">
            		<thead>
                		<form action="{{ route('kategori.store') }}" method="POST">
                		@csrf
                		<thead>
                		<div class="form-group">
                        <label >Id Kategori</label>
                        <input name="id_kategori" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Id Kategori" value="">
                    </div>
                    <div class="form-group">
                    		<label >Nama Kategori</label>
                    		<input name="nama_kategori" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Nama Kategori" value="">
               			 </div>
                    		<button type="submit" class="btn btn-success">Tambah</button>
            			</form>
            		</thead>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection